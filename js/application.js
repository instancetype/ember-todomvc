/**
 * Created by instancetype on 9/30/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
window.Todos = Ember.Application.create()

Todos.ApplicationAdapter = DS.LSAdapter.extend({
  namespace : 'todos-emberjs'
})


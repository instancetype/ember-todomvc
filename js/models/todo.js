/**
 * Created by instancetype on 9/30/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
Todos.Todo = DS.Model.extend(
  { title : DS.attr('string')
  , isCompleted : DS.attr('boolean')
  }
)

Todos.Todo.FIXTURES = [

  { id: 1
  , title: 'Make TodoMVC App'
  , isCompleted: true
  }

, { id: 2
  , title: 'Eat Steak Salad'
  , isCompleted: false
  }

, { id: 3
  , title: 'Feed Cats'
  , isCompleted: false
  }
]
/**
 * Created by instancetype on 10/1/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
Todos.TodoController = Ember.ObjectController.extend({

  isCompleted : function(key, value) {
    var model = this.get('model')

    if (value === undefined) {
      return model.get('isCompleted')
    }
    else {
      model.set('isCompleted', value)
      model.save()

      return value
    }
  }.property('model.isCompleted')

, actions : {

    editTodo: function () {
      this.set('isEditing', true)
    }

  , acceptChanges: function () {
      this.set('isEditing', false)

      if (Ember.isEmpty(this.get('model.title').trim())) {
        Ember.run.debounce(this, 'removeTodo', 0)
      }
      else {
        this.get('model').save()
      }
    }

  , removeTodo: function () {
      this.removeTodo()
    }
  }
, removeTodo : function () {
    var todo = this.get('model')
    todo.deleteRecord()
    todo.save()
  }

, isEditing : false
})
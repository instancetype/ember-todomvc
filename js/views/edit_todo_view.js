/**
 * Created by instancetype on 10/1/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
Todos.EditTodoView = Ember.TextField.extend({
  didInsertElement : function() {
    this.$().focus()
  }
})

Ember.Handlebars.helper('edit-todo', Todos.EditTodoView)
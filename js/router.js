/**
 * Created by instancetype on 9/30/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
Todos.Router.map(function() {
  this.resource('todos', { path : '/' }, function() {
    this.route('active')
    this.route('completed')
  })
})

Todos.TodosRoute = Ember.Route.extend({
  model: function () {
    return this.store.find('todo')
  }
})

Todos.TodosIndexRoute = Ember.Route.extend({
  model : function() {
    return this.modelFor('todos')
  }
})

Todos.TodosActiveRoute = Ember.Route.extend({
  model: function() {
    return this.store.filter('todo', function(todo) {
      return !todo.get('isCompleted')
    })
  }

, renderTemplate : function(controller) {
    this.render('todos/index', { controller : controller })
  }
})

Todos.TodosCompletedRoute = Ember.Route.extend({
  model: function() {
    return this.store.filter('todo', function(todo) {
      return todo.get('isCompleted')
    })
  }

, renderTemplate : function(controller) {
    this.render('todos/index', { controller : controller })
  }
})